---
title: "Bionics Bootcamp"
date: 2020-06-27T18:24:14+01:00
author: Brooks J Rady
image: images/blog/gen_banner.jpg
description: "A curriculum covering programming, electronics, and CAD"
---

Many passionate students join Sheffield Bionics every year, but often lack the technical background needed to keep up with the more senior members. With that being said, it's vitally important to us that everyone who wants to take part in Bionics has the means to do so. Bionics Bootcamp is a new program designed to get fresh recruits and senior members alike up to speed with programming, electronics, and mechanical design.

## Bionics Bootcamp Resources

If you are already part of Bionics Bootcamp, you can find more information and resources about the particular lessons on [our dedicated website here](https://sheffield-bionics.gitlab.io/bionics-general/)!

## Who We're Looking For

We are open to anyone with a passion for bionics – regardless of experience or background! New members will be strongly encouraged to go through the first semester of the program, but are free to dip in and out as they see fit. Likewise, existing members of bionics are strongly encouraged to come along and learn something new!

The program also strives to introduce the other sub-teams of Bionics and help indecisive new members find the sub-team(s) best suited to their skills and interests.

## The Plan

### Session Structure

Most sessions will consist of around an hour of interactive instruction followed by an optional second hour of "tutorial" time during which students can ask questions, share ideas, and / or work on projects applying what they've learned. These sessions take place once a week for 10 weeks throughout each semester.

To accommodate learners of all skill-levels, each session will be divided into introductory and advanced topics. The introductory level will be taught in-person while the higher-levels are delivered through video tutorials and worksheets that allow advanced students to set their own pace.

### Semester 1 – The Essentials

The goal of the first semester is to help new members build the foundation they need to meaningfully contribute to the other teams within the society. Each week aims to build on the content of previous sessions, but should still be accessible to students just looking to drop in.

A tentative structure of the 10 week course is laid out below:

1. Programming in Python (1/2)
2. Programming in Python (2/2)
3. Git + Version Control
4. Breadboard Circuit Basics
5. Controlling Hardware with Arduino (1/2)
6. Controlling Hardware with Arduino (2/2)
7. 3D Modelling in Fusion 360 (1/2)
8. 3D Modelling in Fusion 360 (2/2)
9. 3D printing + Manufacturing in the iForge
10. Project Showcase

### Semester 2 – Specialist Topics

While the first semester focuses on progressively building up the knowledge that every team member needs, the second semester is comprised of more stand-alone, specialist topics.

Some things currently being considered for the second semester are:

- Signal Processing & Machine Learning
- Laser Cutter Operation
- PCB Design
- Soldering
- Advanced CAD with GraphCAD Society
