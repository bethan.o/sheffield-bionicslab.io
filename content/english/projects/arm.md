---
title: "Arm Project"
date: 2020-06-29T18:33:06+01:00
author: Thomas Binu Thomas
image: images/blog/arm_banner.jpg
description: "The current prosthetic hand / arm prototype"
---

The Arm Project team is the most advanced of the three teams with the most senior members. The team has been working hard in modelling different arms, using our prior experience building open source hands.

## The Plan

We hold our weekly meetings every Thursday from 6-7 PM during the academic year. This involves a brief update of what is going on and what needs to be done. The team then splits into its groups, hardware and firmware get to work on the project.

The team currently is primarily focused on creating an arm for our Pilot Mark. After this, we will be focusing on the developing other areas of the team. We plan to create more of our own novel arms using different designs, create more custom motor controllers and hopefully create our own EMG sensor array. We have also started collaborating with Enactus to create arm kits that can be sold to raise money. This money will be used to buy hands for amputees who cannot afford current prostheses.

## Who We're Looking For

The team is divided into 2 main groups. The team welcomes any enthusiastic and self-driven student wanting to work in either of the teams. A lot of the knowledge we gain is found as we go, so we are looking for people ready to put in work to research and tackle completely new problems.

### Hardware

- Designing the arm using Fusion 360 after researching various mechanical methods and catering for the arm’s components
- Identifying the major electronic components required to actuate the arm
- Printing the arm using the society’s 3D printer
- Sourcing other materials that are not 3D printed eg. Creating polyurethane moulds
- Assembling the arm with its corresponding actuators
- Improving the design

### Firmware

- Creating electronic schematics and PCBs of the arm using KiCad, Eagle or equivalent software
- Coding for the precise control of the arm including it’s reactions to the environment using an Arduino, Raspberry Pi or equivalent microcontroller
- Characterising data collected from the Myo Band EMG sensor using machine learning techniques in MATLAB or Python
- Using control techniques in MATLAB and MATLAB Simulink to create custom motor controllers

## Sponsorship

As we stated on our current plans, we are looking for sponsorships to currently cover the expenses of the CYBATHLON competition. This includes the money spent building the hand and the competition’s registration fees. We also are looking for sponsors to help fund us to buy more industrial-grade actuators and components to help us take the team’s product up a level.
